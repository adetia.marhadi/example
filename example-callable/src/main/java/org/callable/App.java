package org.callable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Hello world!
 *
 */
public class App 
{
	
    public void app() throws InterruptedException, ExecutionException {
    	ExecutorService exec = Executors.newFixedThreadPool(100);
    	
    	Callable<String> call = new Callable<String>() {
			
			public String call() throws Exception {
				// TODO Auto-generated method stub
				String result = "A";
				System.out.println(result);
				return result;
			}
		};
		
		Callable<String> call2 = new Callable<String>() {
			
			public String call() throws Exception {
				// TODO Auto-generated method stub
				String result = "B";
				System.out.println(result);
				return result;
			}
		};
		
		Future<String> f = exec.submit(call);
		Future<String> g = exec.submit(call2);
		
		System.out.println("C");
    }
}
