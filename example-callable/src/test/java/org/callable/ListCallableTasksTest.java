/**
 * 
 */
package org.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

/**
 * @author adetia.marhadi
 *
 */
public class ListCallableTasksTest {
	
	@Test
	public void main() {
		init();
	}
	
	private void init() {
		
		for (int i = 0; i < 100; i++) {
			
			
			final int x = i;
			Runnable runn = () -> {
				endpoint(x);
			};
			
			runn.run();
			
		}
		
	}
	
	private int endpoint(int data) {
		System.out.println("data " + data);
		return data;
	}

}
