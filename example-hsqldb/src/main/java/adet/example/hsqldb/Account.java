/**
 * 
 */
package adet.example.hsqldb;

import java.math.BigDecimal;

/**
 * @author adetia.marhadi
 *
 */
public class Account {
	
	private BigDecimal balance;

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	

}
