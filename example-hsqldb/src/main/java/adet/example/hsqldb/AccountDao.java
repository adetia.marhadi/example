/**
 * 
 */
package adet.example.hsqldb;

import java.math.BigDecimal;

/**
 * @author adetia.marhadi
 *
 */
public interface AccountDao {
	
	int deduct(final String account, final BigDecimal amount);
	
	Account findBalanceById(final String account);

}
