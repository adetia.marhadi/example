/**
 * 
 */
package adet.example.hsqldb;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author adetia.marhadi
 *
 */
public class AccountDaoImpl implements AccountDao {
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/* (non-Javadoc)
	 * @see adet.example.hsqldb.AccountDao#deduct(java.lang.String)
	 */
	@Override
	public int deduct(String account, BigDecimal amount) {
//		String sql = "SELECT BALANCE FROM T_MST_ACCOUNT WHERE ID = :id FOR UPDATE";
//		
//		Map<String, String> params = new HashMap<>();
//		params.put("id", account);
//		
//		namedParameterJdbcTemplate.queryForObject(sql, params, (rs, row) -> {
//			Account result = new Account();
//			result.setBalance(BigDecimal.valueOf(rs.getDouble(1)));
//			System.out.println("before: " + BigDecimal.valueOf(rs.getDouble(1)));
//			return result;
//		});
		
		System.out.println("deduct: " + amount);
		
		String sql2 = "UPDATE T_MST_ACCOUNT SET BALANCE = BALANCE - :amount WHERE ID = :id";
		
		Map<String, Object> params2 = new HashMap<>();
		params2.put("amount", amount.doubleValue());
		params2.put("id", account);
		
		return namedParameterJdbcTemplate.update(sql2, params2);
	}

	/* (non-Javadoc)
	 * @see adet.example.hsqldb.AccountDao#findBalanceById(java.lang.String)
	 */
	@Override
	public Account findBalanceById(String account) {
		String sql = "SELECT BALANCE FROM T_MST_ACCOUNT WHERE ID = :id FOR UPDATE";
		
		Map<String, String> params = new HashMap<>();
		params.put("id", account);
		
		try {
			return namedParameterJdbcTemplate.queryForObject(sql, params, (rs, row) -> {
				Account result = new Account();
				result.setBalance(BigDecimal.valueOf(rs.getDouble(1)));
				return result;
			});
		} catch (EmptyResultDataAccessException empty) {
			return null;
		}		
	}

}
