/**
 * 
 */
package adet.example.hsqldb;

/**
 * @author adetia.marhadi
 *
 */
@FunctionalInterface
public interface UserDao {
	
	User findByName(final String name);

}
