/**
 * 
 */
package adet.example.hsqldb;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author adetia.marhadi
 *
 */
public class UserDaoImpl implements UserDao {
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public User findByName(String name) {
		String sql = "SELECT NAME, CODE FROM T_MST_USER WHERE NAME = :name LIMIT 1";
		
		Map<String, String> params = new HashMap<>();
		params.put("name", name);
		
		try {
			return namedParameterJdbcTemplate.queryForObject(sql, params, (rs, row) -> {
				User user = new User();
				user.setName(rs.getString(1));
				user.setCode(rs.getString(2));
				return user;
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		
	}

}
