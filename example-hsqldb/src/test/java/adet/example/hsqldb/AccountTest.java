/**
 * 
 */
package adet.example.hsqldb;

import java.math.BigDecimal;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * @author adetia.marhadi
 *
 */
public class AccountTest implements DatabaseConnectionService {
	
	private EmbeddedDatabase db;
	
	@Before
	public void setup() {
		db = connect();
	}
	
//	@Test
	public void deductTest() {		
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
		
		AccountDaoImpl accountDao = new AccountDaoImpl();
		accountDao.setNamedParameterJdbcTemplate(template);
		
		int account = accountDao.deduct("100001", BigDecimal.valueOf(20));
		
		Assert.assertTrue(account > 0);
	}
	
//	@Test
	public void findBalanceByIdTest() throws Exception {
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
		
		AccountDaoImpl accountDao = new AccountDaoImpl();
		accountDao.setNamedParameterJdbcTemplate(template);
		
		Account account = accountDao.findBalanceById("100001");
		
		if (account.getBalance().compareTo(BigDecimal.ZERO) < 0) {
			System.out.println("not enough.");
			throw new Exception();
		}
		
		System.out.println("lastbalance: " + account.getBalance());
		
		Assert.assertNotNull(account);
	}
	
//	@Test
	public void findBalanceByIdNotFoundTest() {
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
		
		AccountDaoImpl accountDao = new AccountDaoImpl();
		accountDao.setNamedParameterJdbcTemplate(template);
		
		Account account = accountDao.findBalanceById("10001");
		
		Assert.assertNull(account);
	}
	
//	@Test
	public void payTest() {
		
		PlatformTransactionManager pl = new DataSourceTransactionManager(db);
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = pl.getTransaction(def);

		
		
		
		try {
			
			findBalanceByIdTest();
			deductTest();
			pl.commit(status);
		} catch (Exception e) {
			pl.rollback(status);
		}
		
		
		
	}
	
	@Test
	public void bulkPay() throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(200);
		
		Callable<Integer> task = () -> {
			try {
				payTest();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 1;
		};
		
		Callable<Integer> task2 = () -> {
			try {
				payTest();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 1;
		};
		
		
		executor.submit(task);
		executor.submit(task);
		executor.submit(task);
		executor.submit(task2);
		executor.submit(task2);
		executor.submit(task);
		executor.submit(task);
		executor.submit(task);
		executor.submit(task2);
		executor.submit(task2);
		executor.submit(task);
		executor.submit(task2);
		executor.submit(task);
		executor.submit(task);
		executor.submit(task);
		executor.submit(task2);
		executor.submit(task2);
		executor.submit(task);
		executor.submit(task);
		executor.submit(task);
		executor.submit(task2);
		executor.submit(task2);
		executor.submit(task);
		executor.submit(task2);
		
		Thread.sleep(5000);
		
	}
	
	@After
	public void close() {
		db.shutdown();
	}

}
