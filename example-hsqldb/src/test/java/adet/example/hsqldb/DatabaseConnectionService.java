/**
 * 
 */
package adet.example.hsqldb;

import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

/**
 * @author adetia.marhadi
 *
 */
public interface DatabaseConnectionService {
	
	default EmbeddedDatabase connect() {
		return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL)
		.addScript("db/sql/create.sql")
		.addScript("db/sql/insert.sql").build();
	}

}
