/**
 * 
 */
package adet.example.hsqldb;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;

/**
 * @author adetia.marhadi
 *
 */
public class HsqlConnectionTest implements DatabaseConnectionService {
	
	private EmbeddedDatabase db;
	
	@Before
	public void setup() {
		db = connect();
	}
	
	@Test
	public void readTest() {		
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
		
		UserDaoImpl userDao = new UserDaoImpl();
		userDao.setNamedParameterJdbcTemplate(template);
		
		User user = userDao.findByName("Adet");
		
		Assert.assertNotNull(user);
		Assert.assertEquals("Adet", user.getName());
	}
	
	@Test
	public void readNotFoundTest() {
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
		
		UserDaoImpl userDao = new UserDaoImpl();
		userDao.setNamedParameterJdbcTemplate(template);
		
		User user = userDao.findByName("Idun");
		
		Assert.assertNull(user);
	}
	
	@After
	public void close() {
		db.shutdown();
	}

}
