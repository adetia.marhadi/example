/**
 * 
 */
package org.example.interfacedemo;

import java.util.Collections;
import java.util.List;

/**
 * @author adetia.marhadi
 *
 */
public interface Student {
	
	public void update();
	
	public default double getGrade() {
		return 10.0;
	}
	
	public abstract String getId();
	
	public static List<String> studentsName() {
		return Collections.emptyList();
	}

}
