/**
 * 
 */
package org.example.interfacedemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**
 * @author adetia.marhadi
 *
 */
public class SubListTest {
	
	@Test
	public void test() {
		
		Map<String, List<String>> bookTicketMap = new HashMap<String, List<String>>();
		/**
		 * tickets.get(0).getTicketNumbers().subList(index, lastIndex)
		 */
		List<String> list = new ArrayList<>();
		list.add("1265734903442");
		
		list.subList(0, 1);
		
		System.out.println(list);
		
		bookTicketMap.put("bebas", list.subList(0, 1));
		
	}

}
