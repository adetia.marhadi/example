/**
 * 
 */
package adet.example.lombok;

import lombok.Data;

/**
 * @author adetia.marhadi
 *
 */

@Data
public class User {
	
	private String name;
	private String code;
	
	public User(String name, String code) {
		this.name = name;
		this.code = code;
	}
	
}
