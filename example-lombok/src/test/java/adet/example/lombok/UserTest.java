/**
 * 
 */
package adet.example.lombok;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author adetia.marhadi
 *
 */
public class UserTest {
	
	@Test
	public void createUserTest() {
		User user = new User("Adet", "100000001");
		
		Assert.assertNotNull(user);
		Assert.assertEquals("Adet", user.getName());
		Assert.assertEquals("100000001", user.getCode());
	}

}
