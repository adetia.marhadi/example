/**
 * 
 */
package adet.example.monad;

import java.util.Optional;

import lombok.Data;

/**
 * @author adetia.marhadi
 *
 */
@Data
public class Car {
	
	private Optional<Insurance> insurance;

}
