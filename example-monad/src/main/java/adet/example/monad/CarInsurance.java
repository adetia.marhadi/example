/**
 * 
 */
package adet.example.monad;

import java.util.Optional;

/**
 * @author adetia.marhadi
 *
 */
public class CarInsurance {

	/**
	 * @param p
	 * @return
	 */
	public String getCarInsuranceName(Optional<Person> p) {
		return p.flatMap(Person::getCar)
				.flatMap(Car::getInsurance)
				.map(Insurance::getName)
				.orElse("UNKNOWN");
	}

}
