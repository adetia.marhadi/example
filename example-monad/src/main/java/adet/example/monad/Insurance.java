/**
 * 
 */
package adet.example.monad;

import lombok.Data;

/**
 * @author adetia.marhadi
 *
 */
@Data
public class Insurance {
	
	private String name;

}
