/**
 * 
 */
package adet.example.monad;

import java.util.Optional;

import lombok.Data;

/**
 * @author adetia.marhadi
 *
 */
@Data
public class Person {
	
	private Optional<Car> car;

}
