/**
 * 
 */
package adet.example.monad;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author adetia.marhadi
 *
 */
public class MonadTest {
	
	@Test
	public void normalTest() {
		Insurance insurance = new Insurance();	
		insurance.setName("ADA");
		
		Optional<Insurance> optInsurance = Optional.of(insurance);
		Car car = new Car();
		car.setInsurance(optInsurance);
		
		Optional<Car> optCar = Optional.of(car);
		Person person = new Person();
		person.setCar(optCar);
		
		Optional<Person> optPerson = Optional.of(person);
		CarInsurance carInsurance = new CarInsurance();
		String carInsuranceName = carInsurance.getCarInsuranceName(optPerson);
		
		Assert.assertNotNull(carInsuranceName);
		Assert.assertEquals("ADA", carInsuranceName);
	}
	
	@Test
	public void unknownTest() {
		Person person = null;
		
		Optional<Person> optPerson = Optional.ofNullable(person);
		CarInsurance carInsurance = new CarInsurance();
		String carInsuranceName = carInsurance.getCarInsuranceName(optPerson);
		
		Assert.assertNotNull(carInsuranceName);
		Assert.assertEquals("UNKNOWN", carInsuranceName);
	}
	
	@Test
	public void nullHandlerTest() {
		Insurance insurance = new Insurance();	
		insurance.setName(null);
		
		Optional<Insurance> optInsurance = Optional.of(insurance);
		Car car = new Car();
		car.setInsurance(optInsurance);
		
		Optional<Car> optCar = Optional.of(car);
		Person person = new Person();
		person.setCar(optCar);
		
		Optional<Person> optPerson = Optional.of(person);
		CarInsurance carInsurance = new CarInsurance();
		String carInsuranceName = carInsurance.getCarInsuranceName(optPerson);
		
		Assert.assertNull(carInsuranceName);
	}

}
