/**
 * 
 */
package org.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author adetia.marhadi
 *
 */
public class Send {

	private static final String QUEUE_NAME = "hello";
	private static Channel channel;
	
	private Send() {}

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		try (final Connection connection = factory.newConnection()) {
			try {
				channel = connection.createChannel();

				channel.queueDeclare(QUEUE_NAME, false, false, false, null);
				String message = "Hello World!";
				channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
				System.out.println(" [x] Sent '" + message + "'");
			} finally {
				channel.close();
			}
		}
	}
}
