package org.redis;

import java.time.Duration;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

/**
 * Hello world!
 *
 */
public class App {

	private static Logger log = Logger.getLogger(App.class);

	private static final String LOCALHOST = "localhost";

	public String connect() {
		try (Jedis jedis = new Jedis(LOCALHOST)) {
			return jedis.ping();
		} catch (Exception e) {
			log.error("failed connected jedis ", e);
			return null;
		}

	}

	private Jedis getConnection() {
		return new Jedis(LOCALHOST);
	}

	public Jedis getJedisConnection() {
		JedisConnection obj = JedisConnection.getInstance();
		try (Jedis jedis = obj.getConnection();) {
			return jedis;
		} catch (Exception e) {
			log.error("get jedis connection pool ", e);
			return null;
		}
	}

	public String connectPool() {
		final JedisPoolConfig poolConfig = buildPoolConfig();

		try (JedisPool jedisPool = new JedisPool(poolConfig, LOCALHOST); Jedis jedis = jedisPool.getResource()) {
			return jedis.ping();
		} catch (Exception e) {
			log.error("failed connected jedis pool ", e);
			return null;
		}

	}

	private Jedis getConnectionPool() {
		final JedisPoolConfig poolConfig = buildPoolConfig();
		try (JedisPool jedisPool = new JedisPool(poolConfig, LOCALHOST)) {
			return jedisPool.getResource();
		} catch (Exception e) {
			log.error("failed connected jedis ", e);
			return null;
		}
	}

	private JedisPoolConfig buildPoolConfig() {
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(128);
		poolConfig.setMaxIdle(128);
		poolConfig.setMinIdle(16);
		poolConfig.setTestOnBorrow(true);
		poolConfig.setTestOnReturn(true);
		poolConfig.setTestWhileIdle(true);
		poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
		poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
		poolConfig.setNumTestsPerEvictionRun(3);
		poolConfig.setBlockWhenExhausted(true);
		return poolConfig;
	}

	public String setKey(String key, String value) {
		try (Jedis jedis = getConnection()) {
			return jedis.set(key, value);
		} catch (Exception e) {
			log.error("set key ", e);
			return null;
		}
	}

	public String getKey(String key) {
		try (Jedis jedis = getConnection()) {
			return jedis.get(key);
		} catch (Exception e) {
			log.error("get key ", e);
			return null;
		}
	}

	public String setList(String key, List<String> value) {
		try (Jedis jedis = getConnectionPool()) {
			if (jedis == null) {
				return null;
			}

			for (String data : value) {
				jedis.lpush(key, data);
			}

			return "OK";
		} catch (Exception e) {
			log.error("set list ", e);
			return null;
		}
	}

	public List<String> getList(String key, int size) {
		List<String> result = null;
		try (Jedis jedis = getConnectionPool()) {
			if (jedis != null) {
				result = jedis.lrange(key, 0, size);
			}

			return result;
		} catch (Exception e) {
			log.error("get list ", e);
			return result;
		}
	}

	public String setSet(String key, Set<String> value) {
		try (Jedis jedis = getConnectionPool()) {
			if (jedis == null) {
				return null;
			}

			for (String data : value) {
				jedis.sadd(key, data);
			}

			return "OK";
		} catch (Exception e) {
			log.error("set list ", e);
			return null;
		}
	}

	public Set<String> getSet(String key) {
		Set<String> result = null;
		try (Jedis jedis = getConnectionPool()) {
			if (jedis != null) {
				result = jedis.smembers(key);
			}

			return result;
		} catch (Exception e) {
			log.error("get list ", e);
			return result;
		}
	}

	public long setHash(String key, String field, String value) {
		try (Jedis jedis = getConnectionPool()) {

			if (jedis == null) {
				return 0;
			}

			return jedis.hset(key, field, value);

		} catch (Exception e) {
			log.error("set hash ", e);
			return 0;
		}
	}

	public String getHash(String key, String field) {
		try (Jedis jedis = getConnectionPool()) {

			if (jedis == null) {
				return null;
			}

			return jedis.hget(key, field);

		} catch (Exception e) {
			log.error("set hash ", e);
			return null;
		}
	}

	public void subscribe() {
		try (Jedis jedis = getConnectionPool()) {

			if (jedis == null) {
				return;
			}

			jedis.subscribe(new JedisPubSub() {
				@Override
				public void onMessage(String channel, String message) {
					log.info(message);
				}
			}, "channel");

		} catch (Exception e) {
			log.error("subscribe ", e);
		}
	}

	public long setHash(Jedis jedis, String key, String field, String value) {

		if (jedis == null) {
			return -1;
		}

		return jedis.hset(key, field, value);
	}

	public String getHash(Jedis jedis, String key, String field) {

		if (jedis == null) {
			return null;
		}

		return jedis.hget(key, field);
	}

}
