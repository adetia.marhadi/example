package org.redis;

import java.time.Duration;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisConnection {
	
	private static Logger log = Logger.getLogger(JedisConnection.class);
	
	private static final String LOCALHOST = "localhost";
	
	private static JedisConnection jedisConnection = new JedisConnection();
	
	private JedisConnection() { }
	
	private JedisPoolConfig buildPoolConfig() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setMaxIdle(128);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        return poolConfig;
    }
	
	public Jedis getConnection() {
		final JedisPoolConfig poolConfig = buildPoolConfig();
    	try (JedisPool jedisPool = new JedisPool(poolConfig, LOCALHOST)) {
    		return jedisPool.getResource();
        } catch (Exception e) {
        	e.printStackTrace();
        	log.error("failed connected jedis ", e);
        	return null;
        }
	}
	
	public static JedisConnection getInstance() {
		return jedisConnection;
	}

}
