package org.redis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import redis.clients.jedis.Jedis;

public class AppTest {
	
	@Test
	public void testConnect() {
		App app = new App();
		Assert.assertEquals("PONG", app.connect());
	}
	
	@Test
	public void testConnectPool() {
		App app = new App();
		Assert.assertEquals("PONG", app.connectPool());
	}
	
	@Test
	public void testSetGetKey() {
		String key = "redis";
		String value = "Hello world!";
		App app = new App();
		String result = app.setKey(key, value);
		Assert.assertEquals("OK", result);
		
		result = app.getKey(key);
		Assert.assertEquals(value, result);
	}
	
	@Test
	public void testSetGetList() {
		String key = "redis-list";
		
		List<String> value = new ArrayList<>();
		value.add("redis");
		value.add("noSql");
		value.add("ada");
		
		App app = new App();
		String result = app.setList(key, value);
		Assert.assertEquals("OK", result);
		
		List<String> resultList = app.getList(key, value.size()-1);
		Assert.assertNotEquals(0, resultList.size());
		
		for (String data : resultList) {
			System.out.println(data);
		}
	}
	
	@Test
	public void testSetGetSet() {
		String key = "redis-set";
		
		Set<String> value = new HashSet<String>();
		value.add("a");
		value.add("b");
		value.add("d");
		value.add("i");
		value.add("n");
		
		App app = new App();
		String result = app.setSet(key, value);
		Assert.assertEquals("OK", result);
		
		Set<String> resultList = app.getSet(key);
		Assert.assertNotEquals(0, resultList.size());
		
		for (String data : resultList) {
			System.out.println(data);
		}
	}
	
	@Test
	public void testSetGetHash() {
		String key = "redis-hash";
		String field = "bulan";
		String value = "maret";
		
		App app = new App();
		long result = app.setHash(key, field, value);
		Assert.assertNotEquals(0, result);
		
		String bulan = app.getHash(key, field);
		Assert.assertEquals(value, bulan);
		
		System.out.println(bulan);
	}
	
	@Test
	public void testSubscribe() {
		App app = new App();
		app.subscribe();
	}
	
	public void testSetHash(App app, Jedis jedis) {
		long result = app.setHash(jedis, "test-hash-adet", "mouse", "iMouse");
		System.out.println("save hash " + result);
		Assert.assertTrue(result > -1);
		
		
	}
	
	public void testGetHash(App app, Jedis jedis) {
		String result = app.getHash(jedis, "test-hash-adet", "mouse");
		System.out.println("value hash " + result);
		Assert.assertEquals("logitech", result);
		
		
	}
	
	@Test
	public void test() {
		App app = new App();
		Jedis jedis = app.getJedisConnection();
		testSetHash(app, jedis);
		testGetHash(app, jedis);
	}

}
