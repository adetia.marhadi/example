/**
 * 
 */
package demo.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * @author adetia.marhadi
 *
 */
public class SeleniumTest {
	
	private static WebDriver driver;
	
	@Before
	public void init() {
		System.setProperty("webdriver.gecko.driver", "/home/nusatrip/Documents/adet/installer/gecko-driver/geckodriver");
		driver = new FirefoxDriver();
	}
	
	@After
	public void destroy() {
		driver.close();
	}
	
	@Test
	public void test() throws InterruptedException {
		//Launch the Online Store Website
		driver.get("http://phptest34.nusadev.com/en/flights");
		
		driver.findElement(By.id("departure-val")).sendKeys("FCO");
		
		driver.findElement(By.id("arrival-val")).sendKeys("_KUL");
		
		driver.findElement(By.id("buttonSearchFlight")).click();
 
        // Print a Log In message to the screen
        System.out.println("Successfully opened the website www.Store.Demoqa.com");
 
		//Wait for 5 Sec
		Thread.sleep(5000);
	}
}
