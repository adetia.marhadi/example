/**
 * 
 */
package adet.example.structuredata;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author adetia.marhadi
 *
 */
public class StructureDataTest {
	
	List<Integer> olds;
	List<Integer> news;
	
	@Before
	public void init() {
		olds = new ArrayList<>();
		olds.add(1);
//		olds.add(2);
//		olds.add(3);
		olds.add(4);
//		olds.add(5);
		olds.add(6);
		
		news = new ArrayList<>();
		news.add(1);
		news.add(2);
		news.add(3);
		news.add(4);
		news.add(5);
		news.add(6);
		news.add(7);
	}
	
	@Test
	public void deletedTest() {
		System.out.println("must deleted >>> ");
		if (olds.removeAll(news)) {
			olds.forEach(System.out::println);
		}
	}
	
	@Test
	public void loadedTest() {
		System.out.println("must loaded >>> ");
		if (news.removeAll(olds)) {
			news.forEach(System.out::println);
		}
	}

}
