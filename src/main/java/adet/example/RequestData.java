/**
 * 
 */
package adet.example;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author adetia.marhadi
 *
 */
public class RequestData {
	
	@JsonProperty("HTTP_CODE")
	private int httpCode;

	public int getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}

}
