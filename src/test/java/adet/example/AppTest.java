package adet.example;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
	
	@Test
	public void test() {
		double a;
		double b = 0.2622412;
		a = b * 100;
		DecimalFormat nf = new DecimalFormat();
		nf.setDecimalSeparatorAlwaysShown(false);
		System.out.println(nf.format(a));
	}
}
