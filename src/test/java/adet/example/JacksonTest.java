/**
 * 
 */
package adet.example;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author adetia.marhadi
 *
 */
public class JacksonTest {
	
	@Test
	public void test() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		RequestData reqData = new RequestData();
		reqData.setHttpCode(200);
		
		String toStr = mapper.writeValueAsString(reqData);
		
		System.out.println("mapper: " + toStr);
	}

}
