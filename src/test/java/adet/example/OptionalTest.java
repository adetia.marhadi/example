/**
 * 
 */
package adet.example;

import java.util.Optional;

import org.junit.Test;

/**
 * @author adetia.marhadi
 *
 */
public class OptionalTest {
	
	@Test
	public void test() {
		String str = "adet";
		String opt = Optional.ofNullable(str).orElseGet(() -> "asd");
		System.out.println(opt);
	}

}
